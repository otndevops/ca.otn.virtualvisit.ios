# ca.otn.virtualvisit.ios

[![CI Status](http://img.shields.io/travis/OTN/ca.otn.virtualvisit.ios.svg?style=flat)](https://travis-ci.org/OTN/ca.otn.virtualvisit.ios)
[![Version](https://img.shields.io/cocoapods/v/ca.otn.virtualvisit.ios.svg?style=flat)](http://cocoapods.org/pods/ca.otn.virtualvisit.ios)
[![License](https://img.shields.io/cocoapods/l/ca.otn.virtualvisit.ios.svg?style=flat)](http://cocoapods.org/pods/ca.otn.virtualvisit.ios)
[![Platform](https://img.shields.io/cocoapods/p/ca.otn.virtualvisit.ios.svg?style=flat)](http://cocoapods.org/pods/ca.otn.virtualvisit.ios)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ca.otn.virtualvisit.ios is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ca.otn.virtualvisit.ios'
```

## Author

OTN, otn_devops@otn.ca

## License

ca.otn.virtualvisit.ios is available under the MIT license. See the LICENSE file for more info.
