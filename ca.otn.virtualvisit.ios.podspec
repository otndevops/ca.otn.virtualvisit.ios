#
# Be sure to run `pod lib lint ca.otn.virtualvisit.ios.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ca.otn.virtualvisit.ios'
  s.version          = '0.1.1'
  s.summary          = 'Connecting OTN partner\'s to the Ontario telemedicine call conferencing network.'
  
  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  
  s.description      = 'Connecting OTN partner\'s to the Ontario telemedicine call conferencing network. '
  
  s.homepage         = 'https://bitbucket.org/otndevops/ca.otn.virtualvisit.ios'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'OTN' => 'otn_devops@otn.ca' }
  s.source           = { :git => 'https://bitbucket.org/otndevops/ca.otn.virtualvisit.ios.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  
  s.ios.deployment_target = '9.0'
  
  # s.source_files = '/Headers/*.{h}'

  s.vendored_frameworks = 'OTNVirtualVisit.framework'

  
  # s.resource_bundles = {
  #   'OTNVirtualVisit' => ['OTNVirtualVisit/Assets/*.png']
  # }
  
  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'UIKit'
  s.dependency 'WebRTC', '~> 63.11.20455'
end
