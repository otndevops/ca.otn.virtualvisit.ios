//
//  OTNVirtualVisit.h
//  OTNVirtualVisit
//
//  Created by Myles Grant on 2018-02-12.
//  Copyright © 2018 Ontario Telemedicine Network. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebRTC/WebRTC.h>
#import <UIKit/UIKit.h>

//! Project version number for OTNVirtualVisit.
FOUNDATION_EXPORT double OTNVirtualVisitVersionNumber;

//! Project version string for OTNVirtualVisit.
FOUNDATION_EXPORT const unsigned char OTNVirtualVisitVersionString[];


#import <OTNVirtualVisit/OTNVirtualVisitConfig.h>


@protocol OTNVirtualVisitDelegate <NSObject>

-(void)onStartCall;
-(void)onEndCall;
-(void)onCallInitiated;
-(void)onCallCancelled;
-(void)onMute;
-(void)onUnmute;
-(void)onCameraDeviceSwitchSuccess:(AVCaptureDevice *)device;
-(void)onCameraDeviceSwitchFailed;
-(void)onAudioOutputDeviceChanged:(NSString *)device;
-(void)onAudioOutputDeviceChangeFailed;
-(void)onAudioInputDeviceChanged:(AVAudioSessionPortDescription *)device;
-(void)onAudioInputDeviceChangeFailed;

@end


@interface OTNVirtualVisit : NSObject <RTCPeerConnectionDelegate>


@property(nonatomic, weak) id<OTNVirtualVisitDelegate> delegate;


-(instancetype)initWithDelegate:(id<OTNVirtualVisitDelegate>)delegate participantVideoView:(RTCEAGLVideoView *)participantVideoView conferenceVideoView:(RTCEAGLVideoView *)conferenceVideoView;


-(void)setAPIURL:(NSString *)url;
-(void)startCall:(NSString *)sessionId;
-(void)endCall;
-(void)cancelCall;
-(void)resumeVideo;
-(void)pauseVideo;
-(void)muteAudioInputDevice;
-(void)unmuteAudioInputDevice;
-(BOOL)isAudioInputDeviceMute;

-(NSArray<AVCaptureDevice *> *)getCameraDevices;
-(void)setCameraDevice:(AVCaptureDevice *)device;
-(AVCaptureDevice *)getCurrentCameraDevice;

-(NSArray<AVAudioSessionPortDescription *> *)getAudioInputDevices;
-(void)setAudioInputDevice:(AVAudioSessionPortDescription *)device;
-(AVAudioSessionPortDescription *)getCurrentAudioInputDevice;

-(NSArray *)getAudioOutputDevices;
-(void)setAudioOutputDevice:(NSString *)device;
-(NSString *)getCurrentAudioOutputDevice;

-(void)addLayoutListener;
-(void)removeLayoutListener;

@end
