//
//  OTNVirtualVisitConfig.h
//  OTNVirtualVisit
//
//  Created by Myles Grant on 2018-02-03.
//  Copyright © 2018 Ontario Telemedicine Network. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OTNVirtualVisitConfig : NSObject



//Default
@property Boolean ENABLE_VIDEO_HW_ACCELERATION;
@property Boolean ENABLE_VIDEO_CALL;
@property int VIDEO_WIDTH;
@property int VIDEO_HEIGHT;
@property int VIDEO_FPS;
@property int VIDEO_MAX_BITRATE;

@property(nonatomic, readonly) int BPS_IN_KBPS;

+(OTNVirtualVisitConfig *_Nonnull)sharedInstance;


@end
